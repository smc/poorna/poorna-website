+++
fragment = "footer"
disabled = true
date = "2022-12-28"
weight = 1200
background = "primary"

menu_title = "Link Title"

[asset]
  title = "Logo Title"
  image = "logo.svg"
  text = "Logo Subtext"
  url = "#"
+++

#### Description Title

Project description such as:
Open source theme for your next project
Provided by Okkur Labs

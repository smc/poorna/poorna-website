+++
fragment = "nav"
#disabled = true
date = "2022-12-27"
weight = 0
#background = ""

[repo_button]
  url = "https://gitlab.com/smc/poorna/"
  text = "Star" # default: "Star"
  icon = "fab fa-gitlab" # defaults: "fab fa-gitlab"

# Branding options
[asset]
  image = "logo.svg"
  text = "Poorna"
+++

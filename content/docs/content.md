+++
fragment = "content"
#disabled = true
date = "2022-12-28"
weight = 100
#background = ""

title = "Documentation"
#subtitle = ""
+++

This keyboard consist of 4 layers. First two layers are the same as normal Inscript or Remington.

```
layer one - Direct key

layer two - shift + key

layer three - AltGr+key (AltGr = Right side Alt)

layer four - AltGr+shift+key
```

You can see the layout image below, in that

- left bottom - layer one
- left top - layer two
- right bottom - layer three
- right top - layer four\
  ![layout_image](/_index/poorna_inscript.png)

### example

In Extended Inscript layout\
In case of 'd',\
'd' gives ്\
'Shift+d' gives അ\
'Right Alt+d' gives ഽ\
'Right Alt+Shift+d' gives ഁ

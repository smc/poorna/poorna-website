+++
fragment = "buttons"
#disabled = false
date = "2025-01-14"
weight = 260
background = "dark"

title = "Printable Layout"
subtitle = "Download Poorna Remington Printable Layout in A3 Express Size"
#title_align = "left" # Default is center, can be left, right or center

[[buttons]]
  text = "Download Svg"
  url = "/files/Remington_layout_a3.svg"
  color = "primary"
  target = "blank"


[[buttons]]
  text = "Download Pdf"
  url = "/files/Remington_layout_a3.pdf"
  color = "primary"
  target = "blank"

+++

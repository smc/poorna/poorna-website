+++
fragment = "content"
#disabled = false
date = "2022-12-27"
weight = 100
background = "light"
content_align = "left"
+++

<div style="margin-bottom:35px">
<h4> Poorna have Extended Inscript and Remington Layouts for Windows, macOS and GNU/Linux and Web. in Android Inscript version is available in <a href="https://indic.app" target="_blank">indic.app.</a> There is a Keyman version too. Which works in every platform.</h4>
</div>

{{<youtube id=pYqK0Mq_Hfk loading=lazy start=30 >}}

<div style="text-align: center;">
<b>Poorna Tutorial from IBComputing.</b>
</div>

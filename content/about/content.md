+++
fragment = "content"
#disabled = true
date = "2017-10-05"
weight = 100
#background = ""

title = "About Poorna"
#subtitle = ""
+++

Poorna is an extended Inscript/Remington keyboard layout which is available in Windows, GNU/Linux, macOS, Android, iOS and Web. It contains all Malayalam Unicode Characters. Read more in Malayalam [smc blog](https://blog.smc.org.in/poorna-malayalam-keyboard-release/)

## Credits

{{< table "table table-striped" >}}
| Role | Name |
|---------|--------|
| Idea | Baiju Muthukadan |
| Project coordination | Jaisen Nedumpala |
| Developer | Mujeeb CPY |
|Academic help | Mahesh Mangalat|
| Logo | Hiran Venugopalan |
| Keyman Port | Ramesh Kunnappully |
| Printable Layout Design| Thomas Vazhappilly & John Thomas Vazhappilly|
| Technical Support | SMC |
| Sponsor for ver 1.0 | [MyG](https://www.myg.in/) [<img src="myg.jpg" width="70"/>](myg.jpg)|
{{</ table >}}

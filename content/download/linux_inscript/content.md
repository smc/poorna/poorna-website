+++
fragment = "content"
date = "2021-09-14"
weight = 130

title = "GNU/Linux Inscript"

[sidebar]
  enable = true
  sticky = true
+++

## Install

To install you can clone the [repo](https://gitlab.com/smc/poorna/poorna-linux) or download the zip.\
To clone and install, follow the steps below in terminal.

```
git clone https://gitlab.com/smc/poorna/poorna-linux.git
cd poorna-linux
sudo ./install.sh
```

## Use

After installation it can be used by adding\
 Malayalam (Poorna Extended Inscript) in gnome-keyboard-properties or in kde keyboard settings. Example in kde is given below.
![kde](poorna-liux-kde.png)

Or use the command to activate the keyboard (It would work only until you reboot, to persist this you have to add it in .bashrc)\
`setxkbmap -model pc105 -layout us,in -variant ,mal_poorna  -option grp:alt_shift_toggle`

+++
fragment = "content"
date = "2021-09-14"
weight = 130

title = "GNU/Linux Remington"

[sidebar]
  enable = true
  sticky = true
+++

To use Extended Remington Layout for Linux, you need IBus in your system.

## Install IBus

Install these packages\
`ibus ibus-m17n m17n-db`
If you are using Ubuntu, run this in terminal\
`sudo apt install ibus ibus-m17n m17n-db`\
For more detailed info refer [swanalekha documentation](https://swanalekha.smc.org.in/desktop/linux/#linux)

## Setting Up IBus

Most GNOME systems are well-integrated with IBus by default.

If you're not using GNOME, you will need to setup IBus. ArchLinux Wiki has good information on setting up IBus: https://wiki.archlinux.org/title/IBus

IBus setup might be a bit tricky in KDE. It can be easily setup in ArchLinux by installing `ibus-input-support`.

## Install Poorna

Clone the [repositoty](https://gitlab.com/smc/poorna/poorna-remington-linux)\
Copy ml-poornaremington.mim to /usr/share/m17n\

It can be done through following commands in terminal

```
git clone https://gitlab.com/smc/poorna/poorna-remington-linux.git
cd poorna-remington-liux
sudo cp ml-poornaremington.mim /usr/share/m17n
```

Then add the layout to your Ibus preferences.\
In Ubuntu you can add keyboard from settings->keyboard

## Remember this

To work 4 layers in Poorna you have to select Right Alt from alternate charecter key option
![ubuntu altgr](altgr-ubuntu.png)

+++
fragment = "content"
date = "2021-09-01"
weight = 150

title = "Windows Inscript"

[sidebar]
  enable = true
  sticky = true
+++

# Install

Download the [Poorna Inscript Windows release](https://gitlab.com/smc/poorna/poorna-releases/-/raw/main/poorna_inscript_windows.zip) file\
Unzip\
Double click setup to install\
Then you can use it from Windows taskbar.

If your Windows system not allowing to install click more info and allow for run.

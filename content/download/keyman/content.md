+++
fragment = "content"
date = "2021-09-01"
weight = 150

title = "Keyman"

[sidebar]
  enable = true
  sticky = true
+++

# Windows

We will use <a href="https://keyman.com" target="_blank">Keyman</a> as the input tool. Keyman input tool is an opensource input mechanism developed by <a href="https://www.sil.org" target="_blank">SIL International</a>. We are offering two **Poorna Malayalam** keyboards for use with Keyman.

1. [**Poorna Malayalam Extended Inscript Keyboard**](https://keyman.com/keyboards/poorna_malayalam_extended_inscript) - Poorna Malayalam Extended Inscript keyboard is a complete Malayalam unicode character set keyboard. The key mapping of this keyboard is an extended version of the Malayalam [Inscript](https://web.archive.org/web/20220302222601/http://malayalam.kerala.gov.in/images/1/16/Inscript.pdf) layout.
2. [**Poorna Malayalam Typewriter Keyboard**](https://keyman.com/keyboards/poorna_malayalam_typewriter) - Poorna Malayalam Typewriter keyboard is a complete Malayalam unicode character set keyboard. The key mapping of this keyboard is an extended version of the Malayalam [Typewriter](https://web.archive.org/web/20230627041037/https://www.typotheque.com/_next/image?url=https%3A%2F%2Fassets.typotheque.com%2Fassets%2FUploads%2Fstories%2Fgodrej-malayalam-typewriter.jpg&w=1920&q=75) layout.

## Download and Install Keyman for Windows with Poorna Malayalam Keyboard

- If Keyman for Windows is not installed, open the keyboard's homepage by clicking on the keyboard name mentioned above.

    <img src="./img/poorna-ml-windows-1.png" style="width:auto">

- Click Install Keyboard button.
- If Keyman for windows already installed, the browser prompt to open a link with Keyman Configuration to install Poorna Malayalam keyboard package. Otherwise click on Download Keyboard Button to download Keyman for Windows and Poorna Malayalam in a single installer.

    <img src="./img/poorna-ml-windows-2.png" style="width:auto">

- Open the folder where the Installer has been downloaded and double-click on the Installer.

    <img src="./img/poorna-ml-windows-3.png" style="width:auto">

- You may be prompted by User Account Control to allow Installer to make changes to your computer -- click 'Yes'.

- The Installation Wizard dialog box will open. Click Install button.

    <img src="./img/poorna-ml-windows-4.png" style="width:auto">

- For help on installing Keyman, please see: <a href="https://help.keyman.com/products/windows/current-version/start/download-and-install-keyman" target="_blank">How to - download and install Keyman</a>.

## Start Keyman.

- To Start Keyman, click on the Windows Search box, type "Keyman", and select Keyman from the list of Apps or double-click on the desktop icon-Keyman.
- Keyman may show a startup screen; simply click Start Keyman to start the app.
- Keyman will now be running and the Keyman icon will be on the Windows Taskbar near the clock.

    <img src="./img/poorna-ml-windows-5.png" style="width:auto">

- If the Keyman icon is not on the Windows Taskbar near the clock, click the arrow or triangle near the clock.

## Turning on Poorna Keyboard by switching Windows input methods

- Open an application to type in, like LibreOffice Writer, Notepad, etc. Note: You should open an application before you select the Poorna Malayalam keyboard, depending on your Windows settings.
- Keyman keyboards are always associated with a Windows language, you can also switch Keyman keyboards using the Windows input methods switcher, located near the clock in the Taskbar.

    <img src="./img/poorna-ml-windows-6.png" style="width:auto">

- The default hotkey for the Windows input methods switcher is Win + spacebar.
- Hold the Win key and press spacebar repeatedly until you reach the Poorna Malayalam keyboard, then release spacebar.
- Start typing; Now you can type in Manglish anywhere and you will see malayalam.

## Turning on Poorna Keyboard from the Keyman Menu

- Open an application to type in, like LibreOffice Writer, Notepad, etc. Note: You should open an application before you select the Poorna Malayalam keyboard, depending on your Windows settings.
- Click on the Keyman icon, on the Windows Taskbar near the clock.

    <img src="./img/poorna-ml-windows-7.png" style="width:auto">

- Select Poorna Malayalam keyboard from the list. You will notice the Keyman menu icon change to the icon of Poorna. This means Poorna keyboard is on.
- Start typing; Now you can type in Manglish anywhere and you will see malayalam.
- Press the Language Switcher hotkey to bring up the Language Switcher. By default, the Language Switcher hotkey is Left Alt + Shift.

## Download and Install Keyman for Windows without any keyboards

- Keyman for Windows can be downloaded without any keyboard layouts like Poorna Malayalam by visiting <a href="https://keyman.com/windows/download" target="_blank">https://keyman.com/windows/download</a>.
- Keyboard layouts can be added after Keyman for Windows has been installed.
- For help on installing Keyman, please see: <a href="https://help.keyman.com/products/windows/current-version/start/download-and-install-keyman" target="_blank">How to - download and install Keyman.</a>

## Downloading & Installing Poorna Keyboard within Keyman

- Keyman for Windows must be installed first.
- Start Keyman.
- Open Keyman Configuration, from the Keyman menu (on the Windows Taskbar near the clock).

    <img src="./img/poorna-ml-windows-8.png" style="width:auto">

- Select the Keyboard Layouts tab.
- Click Download keyboard… at the bottom of the window.

    <img src="./img/poorna-ml-windows-9.png" style="width:auto">

- Type ‘Poorna Malayalam’ into the search box, then click on search button.
- After a moment, Keyman will present ‘Poorna Malayalam Extended Inscript’ and ‘Poorna Malayalam Typewriter’ in list of keyboards. Choose one to install.

    <img src="./img/poorna-ml-windows-10.png" style="width:auto">

- A page with more details about Poorna Malayalam keyboard will be presented. Click Install keyboard in this page to continue downloading and installing the keyboard.

    <img src="./img/poorna-ml-windows-11.png" style="width:auto">

- The Poorna Malayalam keyboard package will be downloaded to your computer.

    <img src="./img/poorna-ml-windows-12.png" style="width:auto">

- Click Install at the Install Keyboard/Package dialog.
- You may be prompted by User Account Control to allow Keyman to make changes to your computer -- click 'Yes'. If you have a separate administrator account -- that is, you are prompted to enter a username and password at this point -- you may need to <a href="https://help.keyman.com/products/windows/current-version/start/configure-computer" target="_blank"> add a language association</a> for your keyboard on your user account after completing the keyboard installation.

    <img src="./img/poorna-ml-windows-13.png" style="width:auto">

- The keyboard will now be installed; you can close Keyman Configuration.

    <img src="./img/poorna-ml-windows-14.png" style="width:auto">

- After successful installaion of Poorna Malayalam keyboard, a welcome screen will be open.

    <img src="./img/poorna-ml-windows-15.png" style="width:auto">

## Installing a Poorna Keyboard from a Folder on Your Computer

- Keyman for Windows must be installed first.
- Download Poorna Malayalam Extended Inscript keyboard package (file name : poorna_malayalam_extended_inscript.kmp) from <a href="https://keyman.com/keyboards/poorna_malayalam_extended_inscript" target="_blank">here</a>.
- Download Poorna Malayalam Typewriter keyboard package (file name : poorna_malayalam_typewriter.kmp) from <a href="https://keyman.com/keyboards/poorna_malayalam_typewriter" target="_blank">here</a>.
- Start Keyman.
- Open Keyman Configuration, from the Keyman menu (on the Windows Taskbar near the clock).
- Select the Keyboard Layouts tab.
- Click Install Keyboard… at the bottom of the window.
- Find and select the keyboard on your computer.
- Click Open.
- Click Install at the Install Keyboard/Package dialog.
- You may be prompted by User Account Control to allow Keyman to make changes to your computer -- click 'Yes'. If you have a separate administrator account -- that is, you are prompted to enter a username and password at this point -- you may need to <a href="https://help.keyman.com/products/windows/current-version/start/configure-computer" target="_blank"> add a language association</a> for your keyboard on your user account after completing the keyboard installation.
- The keyboard will now be installed; you can close Keyman Configuration.
- After successful installaion of Poorna Malayalam keyboard, a welcome screen will be open.

## Typing

Open any application where you want to type Malayalam. From the Keyman icon on the Windows Taskbar or using Windows input methods switcher, Choose "Poorna Malayalam Extended Inscript" or "Poorna Malayalam Typewriter". Now you can type anywhere and you will see malayalam.

<img src="./img/poorna-ml-windows-16.png" style="width:auto">

# Other Operating systems (linux, mac, Ios, Android, Ipad)

the basic idea is download keyman for your platform and then add poorna. you can add a keyboard and can search for poorna. here is the example from android.
<img src="./img/android_poorna.png.jpg" style="width:auto">

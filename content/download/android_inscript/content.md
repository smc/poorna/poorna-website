+++
fragment = "content"
date = "2023-02-13"
weight = 130

title = "Android Inscript"

[sidebar]
  enable = true
  sticky = true
+++

## Install

To install dowload indic keyboard from playstore or F-Droid

[Click here to download from Google playstore](https://play.google.com/store/apps/details?id=org.smc.inputmethod.indic)

[Click here to download from F-Droid](https://f-droid.org/en/packages/org.smc.inputmethod.indic/)

## Use

After installation choose മലയാളം - Poorna Inscript
![choosepoorna](choose_poorna_android.jpg)
